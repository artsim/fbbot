from flask import Flask, request
from wit import Wit
import requests
import itertools
import os
import redis


WIT_TOKEN = '4VT7XABB2DEUST5WUVNP7WWI2UM3USNT'

app = Flask(__name__)

PAT = 'EAAKL5Ietp68BALIzaZAdYoLySvj6dZCSlv8ppZCBBf50ZBW2JXjq3IRS4twk' \
'jzBe8ZBFefLD1qd3s29VozNnRmlFrooGZApuvsftrugTkeANXjjX4K8s1Oa4BLWwQYZ' \
'Cz1ZBy9YXJu02xcJo4sNrTpr0zgrALZCoAKU97VdmROyYWlAZDZD'

#POOL = redis.ConnectionPool(host='127.0.0.1', port=6379, db=0)
POOL = redis.ConnectionPool(host='127.0.0.1', port=19791, db=0)

def getVariable(fb_id, variable_name):
    my_server = redis.Redis(connection_pool=POOL)
    response = my_server.get(fb_id + "-" + variable_name)
    if response == "True":
        response = True
    else:
        response = False
    return response

def setVariable(fb_id, variable_name, variable_value):
    my_server = redis.Redis(connection_pool=POOL)
    my_server.set(fb_id + "-" + variable_name, variable_value)

def deleteAll(fb_id):
    # clear all saved states in redis
    my_server = redis.Redis(connection_pool=POOL)
    my_server.delete("{0}-qr_response".format(fb_id))
    my_server.delete("{0}-menu_response".format(fb_id))
    my_server.delete("{0}-camenu_response".format(fb_id))


@app.route('/.well-known/acme-challenge/<challenge>')
def letsencrypt_check(challenge):
    return send_from_directory('.well-known/acme-challenge',
                               challenge, mimetype='text/plain')


@app.route('/', methods=['GET'])
def handle_verification():
    print "Handling Verification: ->"
    if(request.args.get('hub.verify_token', '') ==
       '9af370947820b108822cd8ab9456b17f56fd7322c5fe55d8a4'):
        print "Verification successful!"
        return request.args.get('hub.challenge', '')
    else:
        print "Verification failed!"
        return 'Error, wrong validation token'


@app.route('/', methods=['POST'])
def handle_messages():
    print "Handling Messages"
    handled_messages = []
    industries = ["Sales","IT","Admin"]
    data = request.json
    if data['object'] == 'page':
        for entry in data['entry']:
            for messaging_event in entry['messaging']:
                if messaging_event.get("message"):
                    messages = entry['messaging']
                    if messages[0]:
                        message = messages[0]
                        fb_id = message['sender']['id']

                        try:
                            quick_reply = message['message']['quick_reply']
                        except KeyError:
                            quick_reply = None

                        try:
                            attachment = message['message']['attachments'][0]
                        except KeyError:
                            attachment = None

                        try:
                            text = message['message']['text']
                        except KeyError:
                            text = " "

                        awaiting_qr_answer = getVariable(fb_id, "qr_response")
                        awaiting_menu_response = getVariable(fb_id, "menu_response")
                        awaiting_camenu_response = getVariable(fb_id, "camenu_response")

                        if(text == "quit" or text == "Quit"):
                            quit_test(fb_id)
                            return "OK", 200
                        elif awaiting_qr_answer and quick_reply is None:
                            text = "Please select an answer for the previous question"
                            send_qr_selection(fb_id, text)
                            return "OK", 200
                        elif awaiting_camenu_response or awaiting_menu_response:
                            send_alternate_menu(fb_id)
                            return "OK", 200
                        else:
                            if quick_reply:
                                print "Handle quick reply"
                                if quick_reply['payload'] == "SELECTED_A":
                                    submit_answer(fb_id, "A")
                                elif quick_reply['payload'] == "SELECTED_B":
                                    submit_answer(fb_id, "B")
                                elif quick_reply['payload'] == "SELECTED_YES":
                                    select_careers(fb_id)
                                elif quick_reply['payload'] == "SELECTED_NO":
                                    text = "Bye! Bye!"
                                    send_message(PAT, fb_id, text)
                                return "OK", 200
                            elif attachment:
                                print "Handle attachment"
                                handle_attachment(attachment, fb_id)
                                return "OK", 200
                            else:
                                print "Handle newly recived message"
                                print message
                                fb_id = message['sender']['id']
                                print fb_id
                                print "Menu response"
                                text = message['message']['text']
                                client.run_actions(session_id=fb_id,
                                                   message=text)
                                # send_message(PAT, fb_id, text)
                                handled_messages.append(
                                    message['message']['mid'])
                            return "OK", 200
                elif messaging_event.get("postback"):
                    # user clicked/tapped "postback" button in earlier message
                    fb_id = messaging_event.get('sender')['id']
                    if messaging_event.get('postback')['payload'] == "GET_STARTED_PAYLOAD":
                        send_menu(fb_id)
                    if messaging_event.get('postback')['payload'] == "CA":
                        selected_test(fb_id)
                    if messaging_event.get('postback')['payload'] == "START_TEST":
                        start_test(fb_id)
                    if messaging_event.get('postback')['payload'] == "RESTART_TEST":
                        restart_test(fb_id)
                    if messaging_event.get('postback')['payload'] == "QUIT_TEST":
                        quit_test(fb_id)
                    if messaging_event.get('postback')['payload'] == "START_MOCK":
                        text = "Sorry this feature is still under development. Check back soon"
                        send_message(PAT, fb_id, text)
                    if messaging_event.get('postback')['payload'] == "START_REVIEW":
                        welcome_message = "To get started with CV review please upload your cv"
                        send_message(PAT, fb_id, welcome_message)
                        #send_industry_menu(fb_id)
                    if(messaging_event.get('postback')['payload']) in industries:
                        industry = messaging_event.get('postback')['payload']
                        submit_industry(fb_id, industry)
                    print "Handle postback"
                    return "OK", 200
                elif messaging_event.get("attachments"):
                    print "Handle attachments"
                    return "OK", 200
                else:
                    print "Handle different event"
                    return "OK", 200


def send_message(token, recipient, text):
    """Send the message text to recipient with id recipient.
    """
    data = {
        'recipient': {'id': recipient},
        'message': {'text': text}
    }
    # Setup the query string with your PAGE TOKEN
    querystring = 'access_token=' + token
    # Send POST request to messenger
    resp = requests.post('https://graph.facebook.com/me/messages?' +
                         querystring,
                         json=data)
    return resp.content


def send_template(token, recipient, jobs):

    payload = []

    for job in jobs:
        payload.append({
            'title': job['title'],
            'subtitle': job['description'],
            'buttons': [
                {
                    'type': 'web_url',
                    'url': job['url'],
                    'title': 'View Job details'
                }
            ]
        })

    data = {
        "recipient": {"id": recipient},
        "message": {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": payload
                }
            }
        }
    }

    querystring = 'access_token=' + token
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    # Send POST request to messenger
    resp = requests.post('https://graph.facebook.com/v2.9/me/messages?' +
                         querystring,
                         json=data, headers=headers)
    return resp.content


def send_menu(recipient):
    username = get_usernamefromid(recipient)

    welcome_message = "Hi {0}. Nice to meet you.\n\nI am here to provide you with personalized career coaching".format(username)
    send_message(PAT, recipient, welcome_message)

    payload = {
        'recipient': {
            'id': recipient
        },
        'message': {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "button",
                    "text":"I can help you with the following. Just select to get started" ,
                    "buttons": [
                        {
                            "type": "postback",
                            "title": "Career Assesment",
                            "payload": "CA"
                        },
                        {
                            "type": "postback",
                            "title": "Review CV",
                            "payload": "START_REVIEW"
                        }
                    ]
                }
            }
        }
    }

    qs = 'access_token=' + PAT
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    # Send POST request to messenger
    resp = requests.post('https://graph.facebook.com/v2.9/me/messages?' + qs,
                         json=payload, headers=headers)

    setVariable(recipient, "menu_response", "True")

def send_alternate_menu(recipient):

    #global awaiting_menu_response
    #global awaiting_camenu_response
    welcome_message = "It seems you did not select any of the menu options. " \
    "Please do so to proceed or type quit to cancel"
    send_message(PAT, recipient, welcome_message)

    awaiting_menu_response = getVariable(recipient, "menu_response")
    awaiting_camenu_response = getVariable(recipient, "camenu_response")

    if awaiting_menu_response:
        buttons = [
                        {
                            "type": "postback",
                            "title": "Career Assesment",
                            "payload": "CA"
                        },
                        {
                            "type": "postback",
                            "title": "Review CV",
                            "payload": "START_REVIEW"
                        }
                    ]
    elif awaiting_camenu_response:
        buttons = [
                        {
                            "type": "postback",
                            "title": "Start Assesment",
                            "payload": "START_TEST"
                        }
                ]

    payload = {
        'recipient': {
            'id': recipient
        },
        'message': {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "button",
                    "text":"Select one to get started" ,
                    "buttons": buttons
                }
            }
        }
    }

    qs = 'access_token=' + PAT
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    # Send POST request to messenger
    resp = requests.post('https://graph.facebook.com/v2.9/me/messages?' + qs,
                         json=payload, headers=headers)

    setVariable(recipient, "menu_response", "True")


def send_button(button_text, button_title, button_payload, recipient):
    payload = {
        'recipient': {
            'id': recipient
        },
        'message': {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "button",
                    "text": button_text,
                    "buttons": [
                        {
                            "type": "postback",
                            "title": button_title,
                            "payload": button_payload
                        }
                    ]
                }
            }
        }
    }

    qs = 'access_token=' + PAT
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    # Send POST request to messenger
    resp = requests.post('https://graph.facebook.com/v2.9/me/messages?' + qs,
                         json=payload, headers=headers)
    return resp.content


def get_username(request):
    context = request['context']
    fb_id = request['session_id']
    resp = requests.get(
        'https://graph.facebook.com/v2.9/%s?fields=first_name&access_token=%s'
        % (fb_id, PAT))
    data = resp.json()
    context['username'] = username = data['first_name']

    welcome_message = "Hi {0}. Nice to see you.\n\nI am here to provide you with personalized career coaching".format(username)
    send_message(PAT, fb_id, welcome_message)

    pt_intro = "I can help you with the following. Just select to get started"
    pt_title = "Career Assesment"
    pt_payload = "CA"
    send_button(pt_intro, pt_title, pt_payload, fb_id)
    return context


def menu(request):
    context = request['context']
    fb_id = request['session_id']

    pt_intro = "I can help you with the following. Just select to get started"
    pt_title = "Career Assesment"
    pt_payload = "CA"
    send_button(pt_intro, pt_title, pt_payload, fb_id)
    return context


def get_usernamefromid(fb_id):
    resp = requests.get(
        'https://graph.facebook.com/v2.9/%s?fields=first_name&access_token=%s'
        % (fb_id, PAT))
    data = resp.json()
    return data['first_name']


def selected_test(fb_id):
    setVariable(fb_id, "menu_response", "False")

    text = "The career assesment test will help you find the right career that matches your unique assessment profile"
    send_message(PAT, fb_id, text)

    pt_intro = "The test consits of 20 questions and takes only 5 minutes to complete"
    pt_title = "Start assesment"
    pt_payload = "START_TEST"
    send_button(pt_intro, pt_title, pt_payload, fb_id)

    setVariable(fb_id, "camenu_response", "True")



def start_test(fb_id):
    setVariable(fb_id, "camenu_response", "False")
    setVariable(fb_id, "menu_response", "False")

    resp = requests.get('http://co.kazibot.com/quiz/start/{0}/'
                        .format(fb_id)).json()
    if resp['status'] == "Success":
        index = resp['index']
        get_question(index, fb_id)

def restart_test(fb_id):
    resp = requests.get('http://co.kazibot.com/quiz/restart/{0}/'
                        .format(fb_id)).json()
    if resp['status'] == "Success":
        index = resp['index']
        get_question(index, fb_id)


def quit(request):
    context = request['context']
    fb_id = request['session_id']
    # clear all saved states in redis
    deleteAll(fb_id)
    text = "Stopped all processing. You can continue by selecting any option on the menu"
    send_message(PAT, fb_id, text)
    return context


def quit_test(fb_id):
    # clear all saved states in redis
    deleteAll(fb_id)
    text = "Stopped all processing. You can continue by selecting any option on the menu"
    send_message(PAT, fb_id, text)

def get_quizlength():
    data = requests.get('http://co.kazibot.com/quiz/1/').json()
    return data['length']


def get_question(index, recipient):
    data = requests.get('http://co.kazibot.com/quiz/question/{0}/{1}/'
                        .format(recipient, index)).json()
    question = data['text']
    first_response = data['responses'][0]['text']
    second_response = data['responses'][1]['text']

    setVariable(recipient, "qr_response", "True")
    send_question(index, question, first_response, second_response, recipient)



def submit_answer(fbid, answer_code):
    try:
        data = requests.get('http://co.kazibot.com/quiz/answer/{0}/{1}/'
                            .format(fbid, answer_code)).json()

        setVariable(fbid, "qr_response", "False")


        if data['status'] == "Complete":
            personality = data['personality']
            message = "Your personality type is {0}".format(personality)
            send_message(PAT, fbid, message)
            description = data['description']
            message = description
            send_message(PAT, fbid, message)
            careerlist = data['careers']
            careers = ', '.join(careerlist)
            message = "Possible career paths include: " + careers
            send_message(PAT, fbid, message)
            message = "Would you like to view available job openings based on " \
                      "your possible career paths?"
            send_message(PAT, fbid, message)
            send_career_selection(fbid, "Please select one")
            # clear all saved states in redis on complete
            deleteAll(fbid)
        else:
            index = data['next_question']
            get_question(index, fbid)
    except Exception:
        pass

def send_question(index, question, first_response, second_response, recipient):
    question = "Q{0}: ".format(index) + question
    send_message(PAT, recipient, question)
    first_response = "A. " + first_response
    send_message(PAT, recipient, first_response)
    second_response = "B. " + second_response
    send_message(PAT, recipient, second_response)
    text = "Select one"
    content = send_qr_selection(recipient, text)
    return content

def send_qr_selection(recipient, text):

    payload = {
        'recipient': {
            'id': recipient
        },
        'message': {
            "text": text,
            "quick_replies": [
                {
                    "content_type": "text",
                    "title": 'A',
                    "payload": "SELECTED_A"
                },
                {
                    "content_type": "text",
                    "title": 'B',
                    "payload": "SELECTED_B"
                }
            ]
        }
    }

    qs = 'access_token=' + PAT
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    # Send POST request to messenger
    resp = requests.post('https://graph.facebook.com/v2.9/me/messages?' + qs,
                         json=payload, headers=headers)
    return resp.content


def select_careers(fb_id):
    data = requests.get('http://co.kazibot.com/quiz/personality/{0}/'
                        .format(fb_id)).json()

    careerlist = data['careers']

    for career in careerlist:
        jobs = get_jobs(career)

        if jobs:
            message = "Current openings found for " + career
            send_message(PAT, fb_id, message)
            send_template(PAT, fb_id, jobs)
        else:
            message = "No current openings found for " + career
            send_message(PAT, fb_id, message)


def get_jobs(career):
    resp = requests.get('http://jobs.kazibot.com/api/jobs?format=json&search=%s'
                        %(career,), auth=('artsim', 'waitisim'))
    data = resp.json()

    #get and pass the first five jobs
    iteration = itertools.imap(None, *([iter(data)] * 5))

    try:
        jobs = next(iteration)
    except StopIteration:
        jobs = data

    return jobs


def send_career_selection(recipient, text):

    payload = {
        'recipient': {
            'id': recipient
        },
        'message': {
            "text": text,
            "quick_replies": [
                {
                    "content_type": "text",
                    "title": 'Yes please',
                    "payload": "SELECTED_YES"
                },
                {
                    "content_type": "text",
                    "title": 'No thanks',
                    "payload": "SELECTED_NO"
                }
            ]
        }
    }

    qs = 'access_token=' + PAT
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    # Send POST request to messenger
    resp = requests.post('https://graph.facebook.com/v2.9/me/messages?' + qs,
                         json=payload, headers=headers)
    return resp.content


def handle_attachment(attachment, fb_id):

    def download_file(url):
        local_filename = url.split('/')[-1].split('?')[0]
        resp = requests.get(url, stream=True)
        with open(local_filename, 'wb') as f:
            for chunk in resp.iter_content(chunk_size=1024):
                if chunk:
                    f.write(chunk)
        return local_filename

    def validate_file_extension(value):
        ext = os.path.splitext(value)[1]  # [0] returns path+filename
        valid_extensions = ['.pdf', '.doc', '.docx']
        if not ext.lower() in valid_extensions:
            return False
        else:
            return True


    filepath = attachment['payload']['url']
    local_file = download_file(filepath)

    supported = validate_file_extension(local_file)

    if supported:
        values = {'fbid': fb_id}
        files = {'datafile': open(local_file, 'rb')}
        resp = requests.post('http://co.kazibot.com/api/resumes/',
                             auth=('artsim', 'waitisim'), files=files, data=values)

        status = resp.status_code

        if status == 201:
            message = "Successfuly uploaded cv. Please select related industry "
            send_industry_menu(fb_id)
        else:
            message = "Upload not successful. Please try uploading your cv again"

        send_message(PAT, fb_id, message)
    else:
        message = 'Unsupported file extension. Please upload a pdf, doc or docx file'
        send_message(PAT, fb_id, message)


def get_resume_details(recipient):
    """
    Fetch resumes details every 5 seconds till we get a completed is true then
    display the results to the user
    """
    import trollius
    from trollius import From

    @trollius.coroutine
    def retry_every_five_seconds():
        keepchecking = True
        while keepchecking:
            resp = requests.get('http://co.kazibot.com/api/resume/{0}/'
                                .format(recipient))
            #resp = requests.get('http://127.0.0.1:8000/api/resume/{0}/'
            content = resp.json()
            status = content["processing"]
            if status == "Completed":
                keepchecking = False
                data = content
            yield From(trollius.sleep(5))
        send_ranking_results(recipient, data)

    loop = trollius.get_event_loop()
    loop.run_until_complete(retry_every_five_seconds())


def send_ranking_results(fb_id, data):
    rank = data['rank']
    count = data['keywords_count']
    message = 'Your results are in. You have a score of {0} based on {1} number'\
    'of keywords found in your CV'.format(rank, count)
    send_message(PAT, fb_id, message)


def send_industry_menu(recipient):

    payload = {
        'recipient': {
            'id': recipient
        },
        'message': {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "button",
                    "text": "Please select the related industry",
                    "buttons": [
                        {
                            "type": "postback",
                            "title": "Sales & Marketing",
                            "payload": "Sales"
                        },
                        {
                            "type": "postback",
                            "title": "IT & Telecoms",
                            "payload": "IT"
                        },
                        {
                            "type": "postback",
                            "title": "Administration",
                            "payload": "Admin"
                        }
                    ]
                }
            }
        }
    }

    qs = 'access_token=' + PAT
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    # Send POST request to messenger
    resp = requests.post('https://graph.facebook.com/v2.9/me/messages?' + qs,
                         json=payload, headers=headers)

def submit_industry(fb_id, industry):
    resp = requests.get('http://co.kazibot.com/api/review/{0}/{1}/'
                        .format(fb_id, industry))
    data = resp.json()
    if data["processing"] == "Started":
        message = "Please wait as I review your CV"
        send_message(PAT, fb_id, message)
        get_resume_details(fb_id)
    else:
        message = "Something seems to have gone wrong. Please select an industry again"
        send_message(PAT, fb_id, message)


def merge(request):
    context = request['context']
    return context


def first_entity_value(entities, entity):
    """
    Returns first entity value
    """
    if entities:
        if entity not in entities:
            return None
        val = entities[entity][0]['value']
        if not val:
            return None
        return val['value'] if isinstance(val, dict) else val
    else:
        return None


def send(request, response):
    """
    Sender function
    """
    # We use the fb_id as equal to session_id
    fb_id = request['session_id']
    text = response['text']
    # send message
    send_message(PAT, fb_id, text)


# Setup Actions
actions = {
    'send': send,
    'merge': merge,
    'send_menu': send_menu,
    'get_username': get_username,
    'send_message': send_message,
    'send_button' : send_button,
    'menu': menu,
    'quit': quit,
}

# Setup Wit Client
client = Wit(access_token=WIT_TOKEN, actions=actions)

if __name__ == '__main__':
    app.debug = True
    app.run()
